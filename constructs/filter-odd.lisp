#!/usr/bin/sbcl --script

(defun filter (n list) 
  ;; Complete this function
	(let ((count 0))
	
	(loop for elem in list do
			( if (evenp count)
				(format t "~d~%" elem)
			)        
			(setq count (+ 1 count))
	)
	) ;;let 
	
)

(defun read-list ()
    (let ((n (read *standard-input* nil)))
        (if (null n)
            nil
            (cons n (read-list)))))

(filter (read) (read-list))

