; to test out tail recursions!

(defun fact-fast (n)
	(fact-fast-aux n 1)	
)

(defun fact-fast-aux (n acc)
	(if (= n 1)
		acc 
	(fact-fast-aux (- n 1) (* acc n))
	)
)


(defun tria-fast(n) ; compute the nth triangular number 1+2+...+n

	(tria-fast-aux n 0)
)


(defun tria-fast-aux (n acc)
	(if (= n 0)
		acc 
	(tria-fast-aux (- n 1) (+ acc n))
	)
)

(defun power-fast(x n)
	(power-fast-aux x n 1)
)

(defun power-fast-aux (x n acc)
	(if (= n 0)
		acc
	(power-fast-aux x (- n 1) (*  x acc))    			
	)
)

(print (fact-fast 5))
(print (tria-fast 5))
(print (power-fast-aux 3 5 1))
