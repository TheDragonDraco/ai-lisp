; recursive function examples on lists
(defun getlen(li)
	(if (eq li ())
	0
	(+ 1 (getlen (cdr li)))		
	)
)

(defun getlen2(li)
	(cond 
		((eq li ()) 0)
		(t (+ 1 (getlen2 (cdr li))))
	)
)
; slow O(n^2) reverse
(defun rev-slow(li)
	(cond 
	((null li) ()) 
	((= (length li) 1) li)
	(t (append (rev-slow (rest li)) (list (first li)) ) )
	) 
)

; fast O(n) reverse
(defun rev-fast (li)
	(rev-fast-aux li nil)
)

(defun rev-fast-aux (li li2)
	(cond
	 ((null li) li2)
	 (t (rev-fast-aux (rest li) (cons (first li) li2)) 	)
	)
)

(defun is-palin(li)
	(eq li (reverse li))
)


; incomplete
(defun is-palin-rec(li)
	(cond
	((null li) t)
	((null (cdr li)) t) ; 1 elem
	
	)

)

(defun makepalin (li) 
	(append li (reverse li))
) 

(setf li '(1 2 3))
(print (getlen li))
(print (getlen2 li)) 
(setf li2 (makepalin li))
(print li2)

(print (rev-fast li))
(print (is-palin li))
(print li2)
(print (is-palin li2))
