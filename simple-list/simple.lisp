;;;

(+ 3 5)
(setf x 1)
(setf y  (= x 1))
(print y)

;(setf x (read))
;(setf y (read))
;(print (+ x y))

(setf li1 '(1 2 3))

(print li1)
(print (car li1))
(print (cdr li1))

(print (cadr li1))
(print (second li1))
(print (butlast li1))

(setf li2 '(2 3 4))
(setf li3 (append li1 li2))
(print li3)

(defun makepalin (li) (append li (reverse li)))
(print (makepalin li1))

(print (cons 1 (cons 2 nil)))
(setf li3 (list 1 2 3 4 5))
(print li3)

(print (eval (quote (+ 1 2))))
(print (first (quote (cons 1 2))))
(print '(+ 1 2))

(set 'x (+ 1 3))
(print x)


(if (= x 3) (print 'x = 3!))
(if (not (= x 3)) (print 'x!=3 ))

(defun squaret(x) (* x x))
(print   (squaret x))
(describe 'squaret)

(defun fact(x)
	(if (= x 1) 
		1
	(* x (fact (- x 1)))
	)
)

(defun len(li)
	(if (null li)
		0
	(+ 1 (len(cdr li)))
	)
) 

(defun fibo(n)
	(if (<= n 2)
		1	
	 
	 (+ (fibo(- n 1)) (fibo(- n 2)))
	)
)   	


(print (fact 3))   
(print (len '(1 2)))
(print (fibo 20))


